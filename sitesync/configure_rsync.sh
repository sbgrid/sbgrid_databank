#!/bin/sh

SBDB_PATH="/public"
SBDB_DOI="10.15785"
SBDB_USER="sbgriddb"
SBDB_GROUP="sbgriddb"

# Step 1: INstall rsyncd and xinetd
function install_rsync()
{
	rpm -q xinetd
	if [[ $? -ne 0 ]]; then
		echo "xinetd was not installed."
		yum -y install xinetd
		chkconfig --add xinetd
		service xinetd start
	fi

	package=`rpm -q rsync`
	if [[ $? -ne 0 ]]; then
		echo "rsync is not installed on this machine." 
		echo "Do you want to install it now (yes|no)?"
		read answer
		if [[ "$answer" == "yes" ]]; then
			yum -y install rsync
			if [[ $? -ne 0 ]]; then 
				echo "Failed to install rpm."
				exit -1
			fi
		else
			echo "Please install rsync package first."
			exit -1
		fi
	else
		echo "Found rsync package: $package"
	fi
}

# Step 2: Enable rsyncd service
function enable_rsync()
{
	echo
	echo "Enable rsync service"
	sed -r 's/(disable)[\t\ ]*=[\t ]*(yes)/\1 = no/' /etc/xinetd.d/rsync > rsync.tmp
	cp rsync.tmp /etc/xinetd.d/rsync 
	cat /etc/xinetd.d/rsync 
	service xinetd restart
}

# Step 3: Create Data Bank Repository Folder
function check_path()
{
	echo
	echo "Please enter a directory for storing the data bank (default: $SBDB_PATH): "
	read dpath
	if [[ -z "$dpath" ]]; then
		dbpath="$SBDB_PATH"
	fi
	SBDB_PATH=$dbpath

	echo "Set the path to data bank storage as $dbpath."
	groupadd $SBDB_GROUP
	useradd -g $SBDB_GROUP $SBDB_USER

	if [[ -d "$dbpath/SBGRID" ]]; then
		return

	elif [[ -d "$dbpath" ]] && [[ -w "$dbpath" ]]; then
		mkdir -p $dpath/SBGRID

	else
		if [[ ! -e "$dbpath" ]]; then
			echo "Directory $dbpath does not exist."

		elif [[ ! -d "$dbpath" ]]; then
			echo "$dbpath is not a directory."

		elif [[ ! -w "$dbpath" ]] || [[ ! -r "$dbpath" ]]; then
			echo "The directory $dbpath need to have both read and write permission."
			echo "You may either switch to the owner of $dbpath or reset its permission."
			ls -al $dbpath
		fi

		exit -1
	fi
}

# Step 4: Configure rsync service
function write_config()
{
	echo
	echo "Write /etc/rsyncd.conf"

	config_file="/etc/rsyncd.conf"
	if [ ! -e "$config_file" ]; then
# create a new configuration fro scratch
cat <<EOF > $config_file
lock file = /var/run/rsync.lock
log file = /var/log/rsyncd.log
pid file = /var/log/rsyncd.pid

use chroot = false

[$SBDB_DOI]
	path = $SBDB_PATH
	uid = $SBDB_USER
	gid = $SBDB_GROUP
	read only = yes
EOF
	else
# check if already configured
		grep $SBDB_DOI $config_file
		if [ $? -eq 1 ]; then
cat <<EOF >> $config_file
[$SBDB_DOI]
	path = $SBDB_PATH
	uid = $SBDB_USER
	gid = $SBDB_GROUP
	read only = yes
EOF
		fi
	fi
}

install_rsync
check_path
write_config
enable_rsync


