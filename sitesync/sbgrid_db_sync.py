#!/bin/env python
import logging, os, socket, subprocess, sys

SBDB_PATH="/public/SBGRID"
SBDB_SITE_PATH="/public/SITES"
SBDB_DOI="10.15785"
SBDB_USER="sbgriddb"
SBDB_GROUP="sbgriddb"
SBDB_SERVER="data.sbgrid.org"
SBDB_DATASET_LIST="    "

class DataSet:
    
    def __init__(self, data_id, basedir=SBDB_PATH):
        self.data_id = data_id
        self.basedir = basedir
    
    def checksum(self):
        os.chdir(os.path.join(self.basedir, self.data_id))
        logging.debug('Performing checksum on ' + self.data_id)
        try:
            subprocess.check_call(['sha1sum', '-c', 'files.sha'])
        except subprocess.CalledProcessError, e:
            if e.returncode:
                return False
        return True
    
    def fetch(self):
        try:
            subprocess.check_call(['rsync', '-a', 'rsync://{0}/{1}/SBGRID/{2}'.format(SBDB_SERVER, SBDB_DOI, self.data_id), self.basedir])
        except subprocess.CalledProcessError, e:
            print e.returncode

class DataSetList:
    
    def __init__(self, full_list_file_path):
        self.listfile =  full_list_file_path
        self.datalist={}
    
    def writeList(self):
        f = open(self.listfile, "w+")
        for d in sorted(self.datalist.keys()):
            if self.datalist[d]:
                f.write("{}\n".format(d))
        f.close()
        logging.info("save dataset list to " + self.listfile )
    
    def readList(self):
        self.datalist={}
        f = open(self.listfile, "r")
        lines = f.readlines()
        for line in lines:
            fs = line.split()
            d = fs[0]
            if d not in self.datalist:
                self.datalist[d] = True
        f.close()
        logging.info("read dataset list from " + self.listfile )
    
    def hasData(self, data_id):
        if data_id in self.datalist:
            return self.datalist[data_id]
        else:
            return False
    
    def addData(self, data_id):
        self.datalist[data_id] = True
        self.writeList()
        logging.info("add dataset " + data_id)
    
class RemoteDataSetList:
    
    def __init__(self, remote_server=SBDB_SERVER):
        self.basedir = os.path.join(SBDB_SITE_PATH, remote_server)
        self.remote_server = remote_server
        
        subprocess.check_call(['mkdir', '-p', self.basedir])
        
        self.exists = False
        self.fetch()
        
        self.data_list = DataSetList(os.path.join(self.basedir, SBDB_DATASET_LIST))
        self.data_list.readList()
    
    def fetch(self):
        try:
            subprocess.check_call(['rsync', '-a', 'rsync://{0}/{1}/SITES/{0}/'.format(self.remote_server, SBDB_DOI), 
                                   os.path.join(SBDB_SITE_PATH, self.remote_server)])
            self.exists = True
        except subprocess.CalledProcessError, e:
            self.exists = False
    
class LocalDataSetList:
    def __init__(self):
        self.hostname = socket.gethostname()
        self.filename = SBDB_DATASET_LIST
        self.local_sitepath = os.path.join(SBDB_SITE_PATH, self.hostname)
        
        self.fullpath = os.path.join(self.local_sitepath, self.filename)
        if not os.path.isdir(self.local_sitepath):
            try:
                print("Path {} does no exist; automtically create the path.".format(self.local_sitepath))
                subprocess.check_call(['mkdir', '-p', self.local_sitepath])
            except subprocess.CalledProcessError:
                sys.exit(-1)
        
        if not os.path.exists(self.fullpath):
            print("Local dataset list {} does not exist under folder {}.".format(self.filename, self.local_sitepath))
            subprocess.check_call(['touch', self.fullpath])
        
        self.data_list = DataSetList(self.fullpath)
        self.data_list.readList()

    def initListFromScan(self):
        os.chdir(SBDB_PATH)
        for d in os.listdir("."):
            logging.info('check dataset ' + d)
            if not self.data_list.hasData(d):
                ds = DataSet(d)
                if ds.checksum():
                    self.data_list.datalist[d] = True
                else:
                    self.data_list.datalist[d] = False
        
        self.data_list.writeList()

def sbdb_sync_by_default():
    try:
        subprocess.check_call(['rsync', '-a', 'rsync://{0}/{1}/SBGRID/'.format(SBDB_SERVER, SBDB_DOI), SBDB_PATH])
    except subprocess.CalledProcessError, e:
        print e.returncode


if __name__ == '__main__':
    
    logging.basicConfig(filename=__file__+".log", level=logging.DEBUG)
    # build local data site list
    local_list = LocalDataSetList()
    local_list.initListFromScan()
    
    # fetch remote data set list
    server_data_list = RemoteDataSetList('data.sbgrid.org')
    server_data_list.fetch()
    if server_data_list.exists:
        # fetch the data based on the list
        for d in server_data_list.data_list.datalist.keys():
            if not local_list.data_list.hasData(d):
                ds = DataSet(d)
                ds.fetch()
                if ds.checksum():
                    local_list.data_list.addData(d)
    else:
        # fetch all datasets on the server
        logging.info("")
        sbdb_sync_by_default()
        local_list.initListFromScan()

