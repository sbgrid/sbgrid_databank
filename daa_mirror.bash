#!/usr/bin/env bash

# example mirroring / checksum validation script

url=data.sbgrid.org

rm -f datasets.lst

# retrieve list of mirrored datasets
rsync rsync://${url}/10.15785/SITES/${url}/datasets.lst datasets.lst

# retrieve datasets
for dset in `cat datasets.lst`
do
	rsync -av rsync://${url}/10.15785/SBGRID/$dset . > ${dset}-dl.out 2> ${dset}-dl.err
	err=$?
	if [ 0 -ne $err ]; then
		echo "problem retrieving dataset $dset"
	else
		echo "dataset $dset retrieved"
	fi
	cd $dset
	shasum -c files.sha > ../${dset}-sha.out 2> ../${dset}-sha.err
	err=$?
	if [ 0 -ne $err ]; then
		echo "problem validating checksum for dataset $dset"
	else
		echo "dataset $dset validated"
	fi
	cd ../
done
