# Structural Biology Data Grid DAA

Information for DAA Tier 1 Centers and Tier 2 Satellites.

Two main components: rsyncd configuration for public/anonymous rsync, and downloading released datasets.

## rsyncd
example configuration file `files/etc/rsyncd.conf` ; allow rsyncd through xinitd if necessary for underlying OS (`files/etc/xinetd.d/rsync`).
`sitesync/configure_rsync.sh` may assist with this configuration.

## downloading datasets
`daa_mirror.bash` will download datasets, and verify checksums.
`sitesync/sbgrid_db_sync.py` is an alternative.

## local access
For making public datasets available for local access and processing, the recommended filepath is `/programs/datagrid/`.  For example, a dataset with a DOI of `10.15785/SBGRID/1` would map to `/programs/datagrid/1` from a compute system.


